﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region camera
    [Header("Camera")]
    [SerializeField] float cameraSpeed_x = 1.0f;
    [SerializeField] float cameraSpeed_y = 1.0f;

    [HideInInspector] Transform cameraTransform;
    #endregion

    #region movement
    [Header("Movement")]
    [SerializeField] float moveSpeed = 3.14f;
    #endregion

    #region ability
    [Header("Ability")]
    [SerializeField] float timeWarpSpeed = 0.5f;

    [HideInInspector] bool is_timeWarped = false;
    #endregion

    #region shooting
    [Header("Shooting")]
    [SerializeField] int ammo;
    [SerializeField] int maxAmmo = 10;
    [SerializeField] int inventory_ammo;
    [SerializeField] float reloadSpeed = 1.0f;
    [SerializeField] float attackSpeed = 0.25f;
    [SerializeField] GameObject projectile;
    [SerializeField] float attackPower = 50.0f;
    [HideInInspector] bool is_fireReady = true;
    [HideInInspector] bool is_reloading = false;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        if (cameraTransform == null)
            cameraTransform = transform.GetChild(0).transform;
        if (cameraTransform == null)
            cameraTransform = Camera.main.transform;

        ammo = maxAmmo;
    }

    // Update is called once per frame
    void Update()
    {
        float m_x = Input.GetAxis("Mouse X");
        float m_y = Input.GetAxis("Mouse Y");

        transform.Rotate(Vector3.up * Time.unscaledDeltaTime * cameraSpeed_x * m_x);
        cameraTransform.Rotate(Vector3.right * Time.unscaledDeltaTime * cameraSpeed_y * m_y);

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        transform.Translate(transform.TransformDirection(new Vector3(h, 0, v)) * moveSpeed * Time.unscaledDeltaTime, Space.World);

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (is_timeWarped)
            {
                Time.timeScale = 1;
                is_timeWarped = false;
            }
            else
            {
                Time.timeScale = timeWarpSpeed;
                is_timeWarped = true;
            }
        }

        if (Input.GetKey(KeyCode.Mouse0) && is_fireReady && !is_reloading && ammo > 0)
        {
            GameObject proj = Instantiate(projectile, cameraTransform.position + cameraTransform.forward, transform.rotation) as GameObject;
            proj.GetComponent<Rigidbody>().AddForce(cameraTransform.forward * attackPower);

            ammo--;

            is_fireReady = false;
            Invoke("SetFireReady", attackSpeed);
        }

        if (Input.GetKeyDown(KeyCode.R) && !is_reloading && inventory_ammo > 0)
        {
            Invoke("SetReloadCompleted", reloadSpeed);
            is_reloading = true;
        }
    }

    private void OnGUI()
    {
        GUI.color = Color.white;

        GUI.Box(new Rect(0, 0, 255, 100), "Inventory:");
        GUI.Label(new Rect(0, 25, 255, 25), "Saved Ammo: " + inventory_ammo.ToString());

        if (is_reloading)
        {
            GUI.Box(new Rect(0, Screen.height - 50, 100, 25), "!!RELOADING!!");
        }

        if (ammo == 0)
            GUI.color = Color.red;
        GUI.Box(new Rect(0, Screen.height - 25, 100, 25), ammo.ToString() + " / " + maxAmmo.ToString());
    }

    void SetFireReady()
    {
        is_fireReady = true;
    }

    void SetReloadCompleted()
    {
        is_reloading = false;

        if (ammo < 0)
            ammo = 0;
        if (ammo > maxAmmo)
            ammo = maxAmmo;
        int diff = maxAmmo - ammo;
        if(inventory_ammo > diff)
        {
            inventory_ammo -= diff;
            ammo = maxAmmo;
            return;
        }
        else
        {
            ammo += inventory_ammo;
            inventory_ammo = 0;
        }
    }
}
